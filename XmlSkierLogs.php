 <?php
/**
  * This file is a part of the code used in IMT2571 Assignment 5.
  *
  * @author Rune Hjelsvold
  * @version 2018
  */

require_once('Club.php');
require_once('Skier.php');
require_once('YearlyDistance.php');
require_once('Affiliation.php');

/**
  * The class for accessing skier logs stored in the XML file
  */  
class XmlSkierLogs
{
    /**
      * @var DOMDocument The XML document holding the club and skier information.
      */  
    protected $node;
	protected $doc;
	protected $xpath;
    
    /**
      * @param string $url Name of the skier logs XML file.
      */  
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
		$this->xpath = new DOMXPath($this->doc);
    }
    
    /**
      * The function returns an array of Club objects - one for each
      * club in the XML file passed to the constructor.
      * @return Club[] The array of club objects
      */
    public function getClubs()
    {
        $clubs = array();
        
        // TODO: Implement the function retrieving club information
		
		$nodes = $this->xpath->query('/SkierLogs/Clubs/Club');
		
		foreach ($nodes as $node) {
			$clubs[] = new Club (
				$node->getAttribute('id'),
				$this->getChildElementValue($node, 'Name'),
				$this->getChildElementValue($node, 'City'),
				$this->getChildElementValue($node, 'County')
			);
		}
        return $clubs;
    }

    /**
      * The function returns an array of Skier objects - one for each
      * Skier in the XML file passed to the constructor. The skier objects
      * contains affiliation histories and logged yearly distances.
      * @return Skier[] The array of skier objects
      */
    public function getSkiers()
    {
        $skiers = array();
    
        // TODO: Implement the function retrieving skier information,
        //       including affiliation history and logged yearly distances.
		
		$nodes = $this->xpath->query('/SkierLogs/Skiers/Skier');
		foreach ($nodes as $node) {
			$skiers[] = new Skier (
				$node->getAttribute('userName'),
				//$this->getSkierAttributes($node, 'userName'),
				//$this->getSkierAttributes($node, 'firstName'),
				//$this->getSkierAttributes($node, 'lastName'),
				//$this->getSkierAttributes($node, 'yearOfBirth')
				$node->getAttribute('firstName'),
				$node->getAttribute('lastName'),
				$node->getAttribute('yearOfBirth')
			);
			//$skiers[4] = $this->getAffiliations($node->getAttribute('userName'));
			//$skiers[5] = $this->getDistances($node->getAttribute('userName'));
		
		}
        return $skiers;
    }
	
	protected function getChildElementValue($root, $attribute)
	{
		$children = $this->xpath->query($attribute, $root)[0]->childNodes;
		$children = $this->xpath->query($attribute, $root)[0]->childNodes;
		if ($children->length == 1) {
			return $children->item(0)->nodeValue;
		} else {
		$res = "";
		for ($i = 0; $i < $children->length; $i++) {
			if ($children->item($i)->nodeType == XML_TEXT_NODE) {
			$res .= $children->nodeValue;
			}
		}
		return $res;
		}
	}
	
	protected function getSkierAttributes($root, $attribute)
	{
        if ($root->nodeName == '$attribute') {
            return $root-item(0)->nodeValue;
    }
	}
	
	public function getAffiliations($userName)
	{
		$affilations = NULL;
		$nodes = $this->xpath->query("Season/Skiers[@clubId and Skier/@userName='$userName']");
		//$nodes = item->getElementByTagName('$userName');
		if ($nodes->length > 0) {
			$affilations = array();
			for ($i = 0; $i < $nodes->length; $i++) {
				$affilations .= $nodes->item($i)->nodeValue;
			}
		}
		return $affilations;
	}
	
	public function getDistances($userName)
	{
		$distances = NULL;
		$nodes = $this->xpath->query("Season/Skiers[@clubId and Skier/@userName='$userName']");
		if ($nodes->length > 0) {
			$distances = array();
			for ($i = 0; $i < $nodes->length; $i++) {
				$distances[$i] = $nodes->item($i)->nodeValue;
			}
		}
		return $distances;
	}
}
?>